#pragma once

#include "anonymous_function.h"
#include "binary_expression.hpp"
#include "function_application.h"
#include "identifier.h"
#include "instruction_fwd.h"

enum MallardInstructionType {
    MID_ASSIGNMENT = 1,
    MID_EXPRESSION = 2,
    MID_IDENTIFIER = 4,
    MID_INT_LITERAL = 8,
    MID_BINARY_EXPRESSION = 16,
    MID_ANONYMOUS_FUNCTION = 32,
    MID_FUNCTION_APPLICATION = 64
};

typedef const char* MallardIntLiteral;

struct MallardAssignment {
    MallardIdentifier identifier;
    MallardInstruction* instruction;
};

struct MallardInstruction {
    union {
        MallardAnonymousFunction anonymous_function;
        MallardFunctionApplication function_application;
        MallardBinaryExpression binary_expression;
        MallardAssignment assignment;
        MallardIdentifier identifier;
        MallardIntLiteral int_literal;
    };
    MallardInstructionType type;
};