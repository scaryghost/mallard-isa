#pragma once

#include "identifier.h"
#include "instruction_fwd.h"
#include "type.h"

#include <stdint.h>

struct MallardAnonymousFunction {
    MallardInstruction* instructions;
    MallardIdentifier* bound_variables;
    MallardType return_type;

    uint32_t n_instructions, n_bound_variables;
};