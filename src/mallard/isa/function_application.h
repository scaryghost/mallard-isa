#pragma once

#include "identifier.h"
#include "instruction_fwd.h"
#include "type.h"

#include <stdint.h>

struct MallardFunctionApplication {
    MallardInstruction* function;
    MallardInstruction* parameters;

    uint32_t n_parameters;
};