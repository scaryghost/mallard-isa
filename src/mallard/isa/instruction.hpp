#pragma once

#include <string>
#include <vector>

namespace mallard {
namespace isa {

/**
 * Supported instructions in the Mallard language.
 * 
 * An instruction is either a statement or expression.  The former 
 * is an instruction whose result can be ignored i.e. it is not meaningful
 * to use the statement's result.  The latter is the opposite where the 
 * expressions's result should be used.
 */
enum class OpCode {
    /**
     * Termianl expression whose result is referenced by a symbolic name.
     * 
     * Variable name is placed in Instruction#literals index 0.  
     * 
     * ```C
     * instruction.literals[0] = variable_name;
     * ```
     */
    VARIABLE,
    /**
     * Terminal expression denoting an int32 in base10.
     * 
     * The base10 digits are placed in Instruction#literals index 0.  
     * 
     * ```C
     * instruction.literais[0] = base10_digits;
     * ```
     */
    INT_LITERAL,
    /**
     * Terminal expression creating a list.
     * 
     * List elements are placed in Instruction#procedures.
     * 
     * ```C
     * instruction.procedures = { &list_expr_1, &list_expr_2, ..., &list_expr_n };
     * ```
     */
    LIST_LITERAL,
    /**
     * Terminal expression creating a map.
     * 
     * Each map entry is treated as a key-value pair in Instruction#procedures where 
     * key_n is at index 2n and value_n is at index 2n + 1.
     * 
     * ```C
     * instruction.procedures = { &key_expr_1, &value_expr_1, &key_expr_2, &value_expr_2, ..., &key_expr_n, &value_expr_n };
     * ```
     */
    MAP_LITERAL,
    /**
     * Terminal expression creating a set.
     * 
     * Set entries are placed in Instruction#procedures.
     * 
     * ```C
     * instruction.procedures = { &set_expr_1, &set_expr_2, ..., &set_expr_n };
     * ```
     */
    SET_LITERAL,
    /**
     * Recursive expression applying a function with 2 productions.
     * 
     * The function name, or symbol, is stored in Instruction#literals index 0 
     * and the left and right expressions in Instruction#produections index 0 and 1 
     * respectively.
     * 
     * ```C
     * instruction.literals = { symbol_or_name };
     * instruction.productions = { &procedure_left, &procedure_right };
     * ```
     * 
     */
    BINARY_EXPRESSION,
    /**
     * Terminal expression defining a function.
     * 
     * A function has a list of bound variables, variables that only exist within the scope of the function, 
     * and a list of productions that comprise the function body.
     * 
     * ```C
     * instruction.literals = { bound_var_1, bound_var_2, ..., bound_var_n };
     * instruction.productions = { &procedure_1, &procedure_2, ..., .&procedure_n };
     * ```
     */
    ANONYMOUS_FUNCTION,
    /**
     * Terminal applying a function to the provided arguments.
     * 
     * Instruction#productions index 0 is the function expression, e.g. function name or anonymous function, 
     * and the other values (index 1 to last) are the function arguments.
     * 
     * ```C
     * instructions.productions = { &function_expression, &parameter_1, &parameter_2, ..., &parameter_n };
     * ```
     */
    FUNCTION_APPLICATION
};

/**
 * Generic graph life structure that defines all Mallard instructions
 * 
 * Instructions are represented as a combination of #literals ( e.g. "x", "3.1415") functioning 
 * as terminal nodes and #productions, other instructions that compose the the top level one.  
 * #opcode determines how the fields are setup, see the #OpCode docs for specifics.
 */
struct Instruction {
    std::vector<std::string> literals;
    std::vector<Instruction*> productions;
    OpCode opcode;
};

}
}