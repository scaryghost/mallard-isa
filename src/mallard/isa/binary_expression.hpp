#pragma once

#include "instruction_fwd.h"

struct MallardBinaryExpression {
    const MallardInstruction* left;
    const MallardInstruction* right;
    char symbol[8];
};